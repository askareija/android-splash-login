package al_khawarizmi.splashlogin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.text.TextUtils
import org.w3c.dom.Text


class MainActivity : AppCompatActivity() {

    private var handler = Handler()
    private var runnable: Runnable = Runnable {
        relay1.visibility = View.VISIBLE
        relay2.visibility = View.VISIBLE
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkInternet()

        btn_login.setOnClickListener {
            if(TextUtils.isEmpty(txt_username.text))
            {
                txt_username.error = resources.getString(R.string.value_required)
                txt_username.requestFocus()
            }
            else
            {
                if(TextUtils.isEmpty(txt_password.text))
                {
                    txt_password.error = resources.getString(R.string.value_required)
                    txt_password.requestFocus()
                }
                else
                {
                    val intent = Intent(this, MenuActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    private fun checkInternet() {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo
        val sessionHelper = SessionHelper()

        if (activeNetwork != null)
        {
            handler.postDelayed(runnable, 2000)
        } else {
            sessionHelper.noInternetDialog(this)
        }

    }
}
