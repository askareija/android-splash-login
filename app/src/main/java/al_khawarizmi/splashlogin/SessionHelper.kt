package al_khawarizmi.splashlogin

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.app.Activity

class SessionHelper {
    fun noInternetDialog(con: Context) {
        val builder = AlertDialog.Builder(con)
        val positiveButton = DialogInterface.OnClickListener { _, _ -> (con as Activity).finish() }

        builder.setMessage("No internet detected. System will terminated.")
                .setTitle("No Internet Connection")
        builder.setPositiveButton(android.R.string.ok, positiveButton)

        val dialog = builder.create()
        dialog.show()
    }
}